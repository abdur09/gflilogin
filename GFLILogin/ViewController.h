//
//  ViewController.h
//  GFLILogin
//
//  Created by Abdur Rahim on 13/05/15.
//  Copyright (c) 2015 Abdur Rahim. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GooglePlus/GooglePlus.h>
@interface ViewController : UIViewController<GPPSignInDelegate,GPPNativeShareBuilder>


@end

