//
//  ViewController.m
//  GFLILogin
//
//  Created by Abdur Rahim on 13/05/15.
//  Copyright (c) 2015 Abdur Rahim. All rights reserved.
//

#import "ViewController.h"
#import <GoogleOpenSource/GoogleOpenSource.h>
#import <GooglePlus/GooglePlus.h>
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)gplusLoginFired:(id)sender {
    
    [GPPSignIn sharedInstance].delegate = self;
    [GPPSignIn sharedInstance].shouldFetchGoogleUserEmail = YES;
    [GPPSignIn sharedInstance].shouldFetchGooglePlusUser = YES;
    [GPPSignIn sharedInstance].shouldFetchGoogleUserID=YES;
    [GPPSignIn sharedInstance].shouldGroupAccessibilityChildren=YES;
    [GPPSignIn sharedInstance].clientID = @"321967730482-js77v9v3openhbfhbc5617une60i5iqe.apps.googleusercontent.com";
    [GPPSignIn sharedInstance].scopes = @[ kGTLAuthScopePlusLogin ];
    [[GPPSignIn sharedInstance] authenticate];
}
#pragma mark - GPPSignInDelegate
- (void)finishedWithAuth:(GTMOAuth2Authentication *)auth
                   error:(NSError *)error {
    NSLog(@"in google plus 2nd func");
    
    if (error) {
        NSLog(@" here %@",
              [NSString stringWithFormat:@"Status: Authentication error: %@", error]);
        return;
    }
    else
    {
        
        GTLServicePlus* plusService ;
        plusService=[GPPSignIn sharedInstance].plusService;
        
        GTLQueryPlus *query = [GTLQueryPlus queryForPeopleGetWithUserId:@"me"];
        query.fields = @"id,emails,image,name,displayName";
        [plusService executeQuery:query
                completionHandler:^(GTLServiceTicket *ticket,
                                    GTLPlusPerson *person,
                                    NSError *error) {
                    if (error) {
                        GTMLoggerError(@"Error:  and here %@", error);
                    } else {
                        NSLog(@" here %@",person.identifier);
                        [self starttologin:person];
                        
                        
                        
                    }
                }];
    }
    
}
-(void)starttologin:(GTLPlusPerson *)logindata
{
    NSLog(@"in google plus 3rd func");
    @try {
        NSLog(@"logindata.userdata: %@ ",logindata);
        NSLog(@"EMAIL IS %@ imageurl is %@ and gender %@ relationship status %@ birthday %@ domain %@ emails %@ identifier %@",[GPPSignIn sharedInstance].userEmail,logindata.image.url,[logindata.gender stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[logindata relationshipStatus],[logindata birthday],[logindata domain],[logindata emails],[logindata identifier]);
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
}


@end
